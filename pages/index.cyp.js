
describe('Ip Tracker - Home', () => {
  it('Ip field should exist', () => {
    cy.visit('/');
    cy.get('[data-cy-element=ip-field]').should('exist');
  });

  it('Ip data should exist', () => {
    cy.get('[data-cy-element=ip-data]').should('exist');
  });

  it('Ip map should exist', () => {
    cy.get('[data-cy-element=ip-map]').should('exist');
  });

  it('Ip field should exist', () => {
    cy.intercept('GET', '**/wookie.codesubmit.io/ipcheck?*', { fixture: 'ip/ipcheck.json' })
      .as('fetchIp');
    cy.get('[data-cy-element=ip-field]').type('8.8.8.8');
    cy.get('[data-cy-element=ip-submit]').click();

    cy.wait('@fetchIp');
  });
});
